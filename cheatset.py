#! /usr/bin/python3

import os
import sys
import json
import logging
import getpass
import simplecrypt
import argparse

logger = logging.getLogger()

STORAGE_BASE = os.path.expanduser('~/.cheatset')

class Storage:
    def __init__(self, name, password):
        self.path = os.path.join(STORAGE_BASE, name)
        self.password = password

    def load(self):
        self._create_if_necessary()
        with open(self.path, 'rb') as fout:
            try:
                raw = fout.read()
                if not raw:
                    return []
                decrypted = simplecrypt.decrypt(self.password, raw) if password else raw
                return eval(decrypted)

            except simplecrypt.DecryptionException:
                logger.error("Encrypted file is corrupt")
                sys.exit(1)

    def save(self, content):
        with open(self.path, 'wb') as fout:
            encrypted = simplecrypt.encrypt(self.password, str(content)) if password else str(content).encode('utf-8')
            fout.write(encrypted)
            fout.flush()

    def _create_if_necessary(self):
        if not os.path.isfile(self.path):
            file = open(self.path, 'a')
            file.close()

class CheatSet:
    def __init__(self, name, password, verbose=False):
        self.cheatset = []
        self.password = password
        self.verbose = verbose
        self.storage = Storage(name, password)

    def load(self):
        self.cheatset = self.storage.load()

    def save(self):
        self.storage.save(self.cheatset)

    def add(self, cheat, keys):
        self.cheatset.append({'text': cheat, 'keys': keys})

    def search(self, terms):
        matching = []
        for cheat in self.cheatset:
            for term in terms:
                if (term in cheat['keys'] or term in cheat['text']) and cheat not in matching:
                    matching.append(cheat)
        return matching

    def _print(self, cheatset):
        for cheat in cheatset:
            if self.verbose:
                print("# Cheat with keys: {}".format(cheat['keys']))
            for line in cheat['text']:
                print(line)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Store cheats, passwords, etc.")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-s", "--store", action="store_true")
    group.add_argument("-r", "--read", action="store_true")
    parser.add_argument("-p", "--plain-store", action="store_true", help="Store without encryption")
    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose output")
    parser.add_argument("cheatset", type=str, help="the cheatset")
    parser.add_argument("keys", type=str, help="what to search for", nargs='+')
    args = parser.parse_args()

    password = getpass.getpass() if not args.plain_store else None

    cheatset = CheatSet(args.cheatset, password, args.verbose)
    cheatset.load()

    if args.read:
        result = cheatset.search(args.keys)
        cheatset._print(result)

    elif args.store:
        cheat = []
        print("Ingrese el cheat: (termina con ^C)")

        while True:
            text_input = input()
            if text_input == "^C":
                break
            cheat.append(text_input)

        cheatset.add(cheat, args.keys)
        cheatset.save()


    sys.exit(1)




