
# CheatSet Store

Manage encrypted cheatsets. Save code, snippets, passwords, or anything.

## Getting Started

Simply clone the project, install dependencies and run !

### Prerequisites

Before running, some dependencies are needed:

```
    sudo apt-get install python3 python3-pip
    sudo pip3 install simple-crypt
```

### Installing

TODO: Configure as a python package.

### Running

You can create as many cheatset libraries as you wish.
Start by saving something.

```
$ ./cheatset.py --store somelibrary key1 key2
Password: 
Ingrese el cheat: (termina con ^C)
line 1
line 2
^C
```
After saving, you can search them like this:
```
$ ./cheatset.py --read somelibrary key1 
Password: 
Results:
=======

# Cheat with keys: ['key1', 'key2']
line 1
line 2
```

## Contributing

Please submit anything you think it will make this thing better. 
